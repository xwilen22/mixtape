//
//  StreetTaggingTests.swift
//  StreetTaggingTests
//
//  Created by Adam Sturesson on 2019-11-27.
//  Copyright © 2019 arlw. All rights reserved.
//

import XCTest
import UIKit
import Foundation
import MapKit
import Network

@testable import StreetTagging

class StreetTaggingTests: XCTestCase {
    func testisEraserActive(){
        GlobalBrushConfiguration.instance.setErasing(to: true)
        let isEraserActive = GlobalBrushConfiguration.instance.isEraserActive()
        XCTAssertTrue(isEraserActive == true)
    }
    func testOfflineMapUrlFetch(){
        let overlay = OfflineMap()
        //If the URL isn't available in the project files it will return a standard URL
        
        let nonExistentUrl:URL = overlay.url(forTilePath: MKTileOverlayPath(x: 12523, y: 61821, z: 1112451, contentScaleFactor: CGFloat()) )
        
        let secondNonExistentUrl:URL = overlay.url(forTilePath: MKTileOverlayPath(x: 12523, y: 611412821, z: 111245312341, contentScaleFactor: CGFloat()) )
        
        let existingUrl:URL = overlay.url(forTilePath: MKTileOverlayPath(x: 2206, y: 1237, z: 12, contentScaleFactor: CGFloat()) )
        XCTAssertEqual(nonExistentUrl,secondNonExistentUrl,"They are not the same!!!")
        XCTAssertNotEqual(nonExistentUrl, existingUrl, "The Urls are the same when they aren't supposed to")
        
    }
    func testSaveTagToCoreData(){
        var saveHandler:TagStoreHandler
        do{
            saveHandler = try TagStoreHandler()
            let tagLocation = CLLocationCoordinate2D(latitude: 17.0, longitude: 5.41)
            saveHandler.saveTagAt(coordinate: tagLocation, tag: UIImage(systemName: "moon")!, realWorldPosition: nil) { (error) in
                XCTFail()
            }
        }catch(let error2){
            XCTFail(error2.localizedDescription)
        }
    }
    func testNetworkCheck(){
        let connection = NetworkConnectivity()
        connection.persistentCheck()
        if(connection.checkIfConnected() == false){
            XCTAssertFalse(connection.checkIfConnected())
        }else{
            XCTAssertTrue(connection.checkIfConnected())
        }
        
    }
    

}
