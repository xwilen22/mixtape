# StreetTagging

Applikationen som ska utvecklas är ett försök att virtualisera street-art (eller graffiti), där personer kan lämna sitt märke i det öppna utan att vandalisera någons egendom. Det som ska utvecklas är en IOS-applikation som tillåter användaren att placera ut ritade bilder på utvalda kartkoordinater inom Jönköpingsområdet, användaren måste vara på eller nära koordinaten för att kunna placera ut bilden. Bilder ritas i applikationen genom att måla på skärmen med fingrarna i ett annat interface eller lägga till en befintlig bild från filsystemet.

Vyn för att måla egna bilder presenteras med ett tomt kanvas och ett verktygsfält  som användaren kan nyttja för att måla bilden. Verktygsfältet består av en färgpalett med förbestämda färger och pensel-typer som bestämmer formen på penseln. Efter man har målat ett verk kan man justera och bekräfta hur taggen ska sitta på t.ex. väggen och därmed sparas positionen och bilden på den lokala maskinen eller så kan man flytta den befintliga taggen eller radera den och börja om från början.

Om användaren väljer att bekräfta taggens position så går det bara att ta bort den om så önskas. 

Kameran på mobiltelefonen nyttjas för att placera ut taggen, där användaren pekar kameran på en vägg eller liknande att placera tagg på och sedan peta på skärmen för att markera var på ytan taggen ska placeras.

Utplacerade taggar sparas lokalt på mobiltelefonen så appen kan användas när telefonen inte är kopplad till internet. Området som kartan täcker är förutbestämd och laddas ned vid första starten av applikationen, detta gör applikationen helt tillgänglig utan en direkt uppkoppling till internet. Detta innebär också att att användaren kan endast se sina egna tags, eftersom vi inte har har några planer på att sätta upp en server under projektets gång.

Användaren kan titta på alla befintliga taggar oavsett om denne är på plats där taggar finns eller inte. De kan även visa hela kartan och se vart bilder finns utplacerade med hjälp utav de kartnålar som redan återfinns på kartan. Användaren kan trycka på en nål i kartan för att se hur många taggar som finns vid den platsen, de får sedan gå till platsen för att med hjälp av kameran se de taggar som finns utplacerade där. Användaren kan också ta bort taggar i samma interface som visar alla upplagda taggar. 

Mockup
https://imgur.com/a/o80u3BS