//
//  TagSaveHandler.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-18.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation

import MapKit
import UIKit
import ARKit

import CoreData

//MARK: Structs & Enums
enum TagDataError:Error {
    case failedToInitialize
    case failedToSaveToDocuments
    case missingPersistentContainerContext
    case failedToSaveToCoreData
    case failedToFetchData
    case failedToRetrieveImage
}

enum ParseError:Error {
    case failedToCastId
}

struct TagData {
    let tagId:UUID
    let posCoordinate:CLLocationCoordinate2D
    let sourceImage:UIImage
    let posRealWorld: SCNVector3
    init (image:UIImage, mapCoordinate:CLLocationCoordinate2D, realWorldPosition:SCNVector3?, id:UUID) {
        self.sourceImage =  image
        self.posCoordinate = mapCoordinate
        self.posRealWorld = realWorldPosition ?? SCNVector3(0,0,0)
        self.tagId = id
    }
}

enum TagDataAttributes:String {
    case longitude = "posLongitude"
    case latitude = "posLatitude"
    case imagePath = "sourceImagePath"
    case entityId = "tagId"
}

//MARK: - TagStoreHandler
class TagStoreHandler {
    let fileManager:FileManager
    let dataPath:URL
    
    let containerViewContext:NSManagedObjectContext
    
    init() throws{
        self.fileManager = FileManager.default
        
        //Gets path to documents folder in the scope of the userdomain
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        guard let documentURL = URL(string: documentDirectory) else {
            throw TagDataError.failedToInitialize
        }
        //
        self.dataPath = documentURL.appendingPathComponent("SavedTags", isDirectory: true)
        if !FileManager.default.fileExists(atPath: self.dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: self.dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                throw TagDataError.failedToInitialize
            }
        }
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            throw TagDataError.failedToInitialize
        }
        self.containerViewContext = delegate.persistentContainer.viewContext
    }
    //MARK: - CRUD Operations
    //MARK: Create
    func saveTagAt(coordinate:CLLocationCoordinate2D, tag tagImage:UIImage, realWorldPosition:SCNVector3?, completion: @escaping(TagDataError) -> Void) {
        let uniqueId = UUID()
        let imageName:String = "\(uniqueId).png"
        
        let imageData = tagImage.pngData()
        
        let fullPathToImage = self.dataPath.absoluteString + "\(imageName)"
        
        //If file is unable to be created the function returns false
        if !self.fileManager.createFile(atPath: (fullPathToImage), contents: imageData, attributes: [FileAttributeKey.type:"png"]) {
            completion(.failedToSaveToDocuments)
            return
        } else {
            //Core data
            guard let tagEntity = NSEntityDescription.entity(forEntityName: "Tag", in: self.containerViewContext) else {
                completion(.failedToSaveToCoreData)
                return
            }
            
            let newTag = NSManagedObject(entity: tagEntity, insertInto: self.containerViewContext)
            
            newTag.setValuesForKeys([
                TagDataAttributes.longitude.rawValue:Double(coordinate.longitude),
                TagDataAttributes.latitude.rawValue:Double(coordinate.latitude),
                TagDataAttributes.imagePath.rawValue:String(fullPathToImage),
                TagDataAttributes.entityId.rawValue:uniqueId
            ])
            
            do {
                try self.containerViewContext.save()
            } catch {
                completion(.failedToSaveToCoreData)
            }
        }
    }
    //MARK: Delete operations
    func deleteAllTags(completion: @escaping(TagDataError) -> Void) {
        let allTagsRequest = NSFetchRequest<NSManagedObject>(entityName: "Tag")
        do {
            let data = try self.containerViewContext.fetch(allTagsRequest)
            
            for tag in data {
                let imagePath = tag.value(forKey: TagDataAttributes.imagePath.rawValue) as! String
                if self.fileManager.fileExists(atPath: imagePath){
                    try self.fileManager.removeItem(atPath: imagePath)
                }
                self.containerViewContext.delete(tag)
            }
            try self.containerViewContext.save()
        } catch {
            completion(.failedToFetchData)
        }
    }
    func deleteTagById(tagId:UUID, completion: @escaping(TagDataError) -> Void) {
        
        let specificTagRequest = NSFetchRequest<NSManagedObject>(entityName: "Tag")
        //Specifies which properties to filter by
        specificTagRequest.predicate = NSPredicate(format: "tagId == %@", tagId.uuidString)
        
        do {
            let tag = try self.containerViewContext.fetch(specificTagRequest)[0]
            
            let imagePath = tag.value(forKey: TagDataAttributes.imagePath.rawValue) as! String
            if self.fileManager.fileExists(atPath: imagePath){
                try self.fileManager.removeItem(atPath: imagePath)
            }
            self.containerViewContext.delete(tag)
            try self.containerViewContext.save()
        } catch {
            completion(.failedToFetchData)
        }
    }
    //MARK: Get operations
    func getTagById(tagId:UUID, completion: @escaping(Result<TagData, TagDataError>) -> Void) {
        //Core data
        let allTagsRequest = NSFetchRequest<NSManagedObject>(entityName: "Tag")
        
        allTagsRequest.predicate = NSPredicate(format: "tagId == %@", tagId.uuidString)
        
        do {
            let tag = try self.containerViewContext.fetch(allTagsRequest)[0]
            let returningTag = try _parseAndRetriveCoreDataToTagData(coreDataTagObject: tag)
            
            completion(.success(returningTag))
        } catch {
            completion(.failure(.failedToFetchData))
        }
    }
    func getAllTags(completion: @escaping(Result<[TagData], TagDataError>) -> Void) {
        //Core data
        let allTagsRequest = NSFetchRequest<NSManagedObject>(entityName: "Tag")
        
        do {
            let result = try self.containerViewContext.fetch(allTagsRequest)
            var tagArray:[TagData] = []
            for tag in result {
                do {
                    tagArray.append(try _parseAndRetriveCoreDataToTagData(coreDataTagObject: tag))
                } catch (_) {
                    continue
                }
            }
            completion(.success(tagArray))
        } catch {
            completion(.failure(.failedToFetchData))
        }
    }
    //MARK: - Private Function
    private func _parseAndRetriveCoreDataToTagData(coreDataTagObject:NSManagedObject) throws -> TagData {
        let fetchedCoordinate:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: coreDataTagObject.value(forKey: TagDataAttributes.latitude.rawValue) as? Double ?? Double(0), longitude: coreDataTagObject.value(forKey: TagDataAttributes.longitude.rawValue) as? Double ?? Double(0))
        let fetchedImagePath:String = coreDataTagObject.value(forKey: TagDataAttributes.imagePath.rawValue) as? String ?? String("")
        
        guard let fetchedId:UUID = coreDataTagObject.value(forKey: TagDataAttributes.entityId.rawValue) as? UUID else {
            throw ParseError.failedToCastId
        }
        
        //Fetches image from filesystem
        let fetchedImage:UIImage
        if !self.fileManager.fileExists(atPath: fetchedImagePath) {
            fetchedImage = UIImage(systemName: "questionmark")!
        } else {
            fetchedImage = UIImage(contentsOfFile: fetchedImagePath)!
        }
        return TagData(image: fetchedImage, mapCoordinate: fetchedCoordinate, realWorldPosition: SCNVector3(0,0,0), id: fetchedId)
    }
}

