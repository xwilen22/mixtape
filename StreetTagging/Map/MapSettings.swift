//
//  MapSettings.swift
//  StreetTagging
//
//  Created by Adam Sturesson on 2019-12-03.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation
import MapKit

class MapSettings {
    let minZoomDistance: Int = 9000
    let maxZoomDistance: Int = 40000
    let mapView:MKMapView
    let initialRegion:MKCoordinateRegion
    let overlay: OfflineMap
    
    init(mapView:MKMapView,initialRegion:MKCoordinateRegion,overlay:OfflineMap){
        self.mapView = mapView
        self.initialRegion = initialRegion
        self.overlay = overlay
    }
    init() {
        self.mapView = MKMapView()
        self.initialRegion = MKCoordinateRegion()
        self.overlay = OfflineMap()
    }
    func customMapSettings(){
        mapView.cameraZoomRange = MKMapView.CameraZoomRange(minCenterCoordinateDistance: CLLocationDistance(minZoomDistance), maxCenterCoordinateDistance: CLLocationDistance(maxZoomDistance))
        mapView.cameraBoundary = MKMapView.CameraBoundary(coordinateRegion: initialRegion)
        mapView.addOverlay(overlay, level: .aboveLabels)
    }
    func normalMapSettings(region: MKCoordinateRegion){
        mapView.removeOverlay(overlay)
        mapView.cameraZoomRange = MKMapView.CameraZoomRange()
        mapView.cameraBoundary = MKMapView.CameraBoundary(coordinateRegion: region)
    }
    
    
}
