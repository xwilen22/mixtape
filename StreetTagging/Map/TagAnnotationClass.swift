//
//  tagAnnotationClass.swift
//  StreetTagging
//
//  Created by Rasmus Andersparr on 2019-11-22.
//  Copyright © 2019 arlw. All rights reserved.
//
import UIKit
import CoreLocation
import MapKit

class TagAnnotation: NSObject, MKAnnotation{
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    let tagImage:UIImage

    init(coordinate: CLLocationCoordinate2D, tagImage:UIImage, title: String, subtitle: String){
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.tagImage = tagImage
    }
}
