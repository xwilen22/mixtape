//
//  offlineMap.swift
//  StreetTagging
//
//  Created by Adam Sturesson on 2019-11-10.
//  Copyright © 2019 arlw. All rights reserved.
//
import MapKit
import Foundation
class OfflineMap:MKTileOverlay {
    override func url(forTilePath path: MKTileOverlayPath) -> URL {
        let tilePath = Bundle.main.url(forResource: "\(path.y)", withExtension: "png", subdirectory: "JKPGTiles/\(path.z)/\(path.x)")
        
        guard let tile = tilePath else {
            return Bundle.main.url(
                forResource: "618",
                withExtension: "png",
                subdirectory: "JKPGTiles/11/1103",
                localization: nil)!
        }
        return tile
    }
}
