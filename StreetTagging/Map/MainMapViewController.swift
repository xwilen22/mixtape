//
//  MapViewController.swift
//  StreetTagging
//
//  Created by Rasmus Andersparr on 2019-11-12.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var userLocateButton: UIButton!
    
    //MARK: - Properties
    var tileRenderer:MKTileOverlayRenderer?
    let initialRegion:MKCoordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 57.7871, longitude: 14.1332), latitudinalMeters: 5000, longitudinalMeters: 5000 )
    var regionAroundUser:MKCoordinateRegion = MKCoordinateRegion()
    var customTilesHasLoaded:Bool = false
    let overlay = OfflineMap() //Fetches all the png files in the JKPG tiles folder and makes an overlay of them
    let locationManager = LocationChecker()
    let userLocation = MKUserLocation()
    var pinArray: [MKAnnotation] = []
    var mapSettings:MapSettings = MapSettings()
    //MARK: - MapView
    override func viewDidLoad() {
        super.viewDidLoad()

        mapSettings = MapSettings(mapView: self.mapView, initialRegion: self.initialRegion, overlay: self.overlay)
        locationManager.initializeInstructions()
        userLocateButton.layer.cornerRadius = 0.5 * userLocateButton.bounds.size.height
        
        _getAndSetSavedTags()
        
        // Sets the mapView properties
        locationManager.setMapToUserLocation(mapView: self.mapView, initialRegion: initialRegion)
        mapView.isRotateEnabled = false
        mapView.delegate = self
        locationManager.delegate = self
        
        _registerMapViews()
        if NetworkConnectivity.instance.checkIfConnected() == false{
            mapSettings.customMapSettings()
        }else{
            mapSettings.normalMapSettings(region:regionAroundUser)
        }
    }
    //MARK: Action Events
    @IBAction func locateUserClicked(_ sender: Any) {
        locationManager.setMapToUserLocation(mapView: self.mapView,initialRegion: initialRegion)
    }
    @IBAction func unwindToMapViewController(_ unwindSegue: UIStoryboardSegue) {}
    //MARK: Private Functions
    private func _getAndSetSavedTags() {
        guard let tagSaveHandler = try? TagStoreHandler() else {
            DispatchQueue.main.async {
                self.present(GenericErrorHandler(title: "Failed to retrieve tags", message: "Could not initialize store handler").getAlert(), animated: true, completion: nil)
            }
            return
        }
        tagSaveHandler.getAllTags(completion: { result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    self.present(GenericErrorHandler(title: "Failed to retrieve tags", message: error.localizedDescription).getAlert(), animated: true, completion: nil)
                }
                break
            case .success(let retrievedTags):
                for tagData in retrievedTags {
                    self.pinArray.append(TagAnnotation(coordinate: tagData.posCoordinate, tagImage: tagData.sourceImage, title: "Tag", subtitle: ""))
                }
                DispatchQueue.main.async {
                    self.mapView.addAnnotations(self.pinArray)
                }
                break
            }
        })
    }
    private func _registerMapViews(){
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: "Pin")
    }
    //MARK: Public Functions
    func addAnnotationAtCurrentLocation(with tagImage:UIImage) {
        guard let currentLocation = locationManager.location?.coordinate else {
            self.present(GenericErrorHandler(title: "Failed to save tag", message: "Could not get current coordinates").getAlert(), animated: true, completion: nil)
            return
        }
        guard let tagSaveHandler = try? TagStoreHandler() else {
            self.present(GenericErrorHandler(title: "Failed to save tag", message: "Could not initialize store handler").getAlert(), animated: true, completion: nil)
            return
        }
        tagSaveHandler.saveTagAt(coordinate: currentLocation, tag: tagImage, realWorldPosition: nil, completion: { saveError in
            print(saveError.localizedDescription)
        })
        
        let userAnnotation = TagAnnotation(coordinate: currentLocation, tagImage: tagImage ,title: "Tag", subtitle: "")
        pinArray.append(userAnnotation)
        mapView.addAnnotations(pinArray)
        
        
    }
}
//MARK: - MapviewDelegate
extension MapViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if NetworkConnectivity.instance.checkIfConnected() == false{
            self.overlay.canReplaceMapContent = true // Its needed to make it posssible for the overlay to replace the current map
            tileRenderer = MKTileOverlayRenderer(tileOverlay: self.overlay)
            return tileRenderer!
        }
        return mapView.renderer(for: overlay) ?? MKOverlayRenderer()
    }
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        if NetworkConnectivity.instance.checkIfConnected() == false && customTilesHasLoaded == false{
            mapSettings.customMapSettings()
            customTilesHasLoaded = true
        }else if NetworkConnectivity.instance.checkIfConnected() == true && customTilesHasLoaded == true {
            regionAroundUser = MKCoordinateRegion(center: locationManager.theUserLocationisEnabled() ?? CLLocationCoordinate2D(), latitudinalMeters: 20000, longitudinalMeters: 20000)
            mapSettings.normalMapSettings(region:regionAroundUser)
            customTilesHasLoaded = false
        }
    }
    func mapView(_ mapView:MKMapView, viewFor annotation:MKAnnotation)->MKAnnotationView?{
        guard let annotation = annotation as? TagAnnotation else{return nil}
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView{
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x:-5, y:5)
            view.glyphImage = annotation.tagImage

            let imageView:UIImageView = UIImageView(image: annotation.tagImage)
            imageView.center = CGPoint(x: view.bounds.size.width * 0.2, y: view.bounds.size.height * 0.2)
            
            view.autoresizesSubviews = true
            view.contentMode = .scaleAspectFit
            view.frame = CGRect(x: 0, y: 0, width: view.frame.width/2, height: view.frame.height/2)
            
            view.detailCalloutAccessoryView = imageView
        }
        return view
    }
    
}
//MARK: - MapviewControllerCLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == .authorizedAlways || status == .authorizedWhenInUse){
            self.userLocateButton.isEnabled = true
            self.locationManager.setMapToUserLocation(mapView: self.mapView, initialRegion: self.initialRegion)
        }else{
            self.userLocateButton.isEnabled = false
        }
    }
}
