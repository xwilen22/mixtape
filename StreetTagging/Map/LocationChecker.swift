//
//  locationChecker.swift
//  StreetTagging
//
//  Created by Adam Sturesson on 2019-12-02.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationChecker: CLLocationManager {
    override init(){
        super.init()
    }
    func initializeInstructions(){
        super.requestWhenInUseAuthorization()
        super.desiredAccuracy = kCLLocationAccuracyBest
        super.distanceFilter = kCLDistanceFilterNone
        super.startUpdatingLocation()
    }
    func theUserLocationisEnabled() -> CLLocationCoordinate2D?{
        return super.location?.coordinate
    }
    func setMapToUserLocation(mapView:MKMapView,initialRegion:MKCoordinateRegion){
        guard let currentUserLocation = super.location?.coordinate else{
            mapView.region = initialRegion
            return
        }
        mapView.region =  MKCoordinateRegion(center: currentUserLocation , latitudinalMeters: 10000, longitudinalMeters: 10000)
    }
}

