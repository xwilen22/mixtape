//
//  CheckIfUserConnectedToNetwork.swift
//  StreetTagging
//
//  Created by Adam Sturesson on 2019-11-18.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation
import Network

class NetworkConnectivity{
    //MARK: Properties
    static let instance = NetworkConnectivity()
    let monitor:NWPathMonitor
    let queue:DispatchQueue
    var connection:Bool
    
    //MARK: Initializer and functions regarding the networkcheck
    init() {
        self.monitor = NWPathMonitor()
        self.queue = DispatchQueue(label: "Monitor")
        self.connection = true
        // Its just here for testing at the start when the application runs
        persistentCheck()
    }
    
    
    func persistentCheck(){
        //Everytime something affects the internet connection this will set the connection to true if connected otherwise it will get set to false!
        monitor.pathUpdateHandler = { path in
            if path.status == .unsatisfied{
                self.connection = false
            }else if path.status == .satisfied{
                self.connection = true
            }
        }
    }
    func checkIfConnected() -> Bool{
        return self.connection
    }
    func startCheck(){
        // This is needed to start the permanent check with help of A DispatchQueue to see path changes
        self.monitor.start(queue: self.queue)
    }
}
