//
//  GenericErrorHandler.swift
//  StreetTagging
//
//  Created by Rasmus Andersparr on 2019-12-02.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation
import UIKit

class GenericErrorHandler{
    
    var title: String?
    var message: String?
    var alert: UIAlertController
    
    init(title: String?, message: String?){
        self.alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    }
    func getAlert() -> UIAlertController {
        return self.alert
    }
}
