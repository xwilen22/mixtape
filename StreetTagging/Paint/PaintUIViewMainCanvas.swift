//
//  PaintUIViewMainCanvas.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-05.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit

class PaintUIViewMainCanvas: UIView {
    struct LinePosition {
        let from:CGPoint
        let to:CGPoint
        
        let styling:BrushSettings
    }
    //All lines in canvas
    var canvasLines:[LinePosition] = []
        
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            print("Could not find context! Aborting draw action")
            return
        }
        
        for line in canvasLines {
            context.move(to: line.from)
            context.addLine(to: line.to)
            
            context.setLineCap(line.styling.brushType)
            context.setLineWidth(line.styling.brushSize)
            context.setStrokeColor(line.styling.brushColour)
            context.strokePath()
        }
    }
    func clearCanvas() {
        canvasLines.removeAll()
        setNeedsDisplay()
    }
    func updateCanvas() {
        setNeedsDisplay()
    }
    func drawLine(from:CGPoint, to:CGPoint, brushConfiguration:BrushSettings) {
        canvasLines.append(LinePosition(from: from, to: to, styling: brushConfiguration))
        
        //Decides which part of the canvas to re-render
        var sizeMarginHeight:CGFloat = brushConfiguration.brushSize, sizeMarginWidth:CGFloat = brushConfiguration.brushSize
        
        if (from.x - to.x) < 0 {
            sizeMarginWidth *= (-1)
        }
        if (from.y - to.y) < 0 {
            sizeMarginHeight *= (-1)
        }
        
        let updateAreaOrigin:CGPoint = CGPoint(x: (from.x + to.x - sizeMarginWidth) / 2, y: (from.y + to.y - sizeMarginHeight) / 2)
        let updateAreaDimension:CGSize = CGSize(width: (from.x - to.x) + sizeMarginWidth, height: (from.y - to.y) + sizeMarginHeight)
        
        let updateArea:CGRect = CGRect(origin: updateAreaOrigin, size: updateAreaDimension)
        
        setNeedsDisplay(updateArea)
    }

}
