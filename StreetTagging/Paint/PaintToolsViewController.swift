//
//  PaintToolsViewController.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-04.
//  Copyright © 2019 arlw. All rights reserved.
//
import UIKit

class PaintToolsViewController: UIViewController {
    let availableBrushes:[BrushItem] = [
        BrushItem(titleName: "Round", type: .round),
        BrushItem(titleName: "Square", type: .square),
        BrushItem(titleName: "Butt", type: .butt)
    ]
    
    var previewSettings:BrushSettings = GlobalBrushConfiguration.instance.getBrushConfiguration()

    @IBOutlet weak var brushSelector: BrushSelectorUISegmentedControl!
    @IBOutlet weak var brushSizeUISlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var brushIndex:Int = 0
        for (index, brush) in availableBrushes.enumerated() {
            if brush.type == previewSettings.brushType {
                brushIndex = index
            }
        }
        
        brushSelector.setBrushes(items: availableBrushes, selectedIndex: brushIndex)
        brushSizeUISlider.setValue(Float(previewSettings.brushSize), animated: true)
    }
    //MARK: - Control Events
    @IBAction func buttonDismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func paintBrushChanged(_ sender: UISegmentedControl) {
        previewSettings.brushType = availableBrushes[sender.selectedSegmentIndex].type
        GlobalBrushConfiguration.instance.setBrushType(newType: availableBrushes[sender.selectedSegmentIndex].type)
    }
    @IBAction func brushSizeChanged(_ sender: UISlider) {
        previewSettings.brushSize = CGFloat(sender.value)
        GlobalBrushConfiguration.instance.setBrushSize(newSize: CGFloat(sender.value))
    }
    
    //Blocks touch controls occuring on parent view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}
