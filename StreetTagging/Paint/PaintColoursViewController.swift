//
//  PaintColoursViewController.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-04.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit

class PaintColoursViewController: UIViewController {
    @IBOutlet weak var colourCollection: UICollectionView!
    
    var defaultColours:[UIColor] = [
        UIColor.black,
        UIColor.white,
        UIColor.blue,
        UIColor.brown,
        UIColor.red
    ]
    var colourScheme:[ColourDetail] = [ColourDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colourCollection.delegate = self
        colourCollection.dataSource = self
        
        let request:ColourSchemeRequest = ColourSchemeRequest(mode: "monokai", colourLimit: 6)
        
        request.getColourScheme{ [weak self] result in
            switch result {
            case .success(let results):
                self?.colourScheme = results
                print(results)
            case .failure(let error):
                print(error)
            }
        }
    }
    @IBAction func buttonDismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

    //Blocks touch controls occuring on parent view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PaintColoursViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return defaultColours.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = colourCollection.dequeueReusableCell(withReuseIdentifier: "ColourCell", for: indexPath) as? UICollectionViewCell {
            cell.contentView.backgroundColor = defaultColours[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalBrushConfiguration.instance.setBrushColour(newColour: defaultColours[indexPath.row].cgColor)
    }
}
