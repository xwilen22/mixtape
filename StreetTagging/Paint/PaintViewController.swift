//
//  PaintViewController.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-04.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit

class PaintViewController: UIViewController {
    
    @IBOutlet weak var UIViewMainCanvas: PaintUIViewMainCanvas!
    var mainCanvas:PaintUIViewMainCanvas!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainCanvas = UIViewMainCanvas
    }
    @IBAction func clearCanvasClicked(_ sender: Any) {
        mainCanvas.clearCanvas()
    }
    //MARK: - Touch Controls
    var touchStartingPosition:CGPoint = CGPoint.zero
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchStartingPosition = touches.first!.location(in: mainCanvas)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchNewPosition = touches.first!.location(in: mainCanvas)
                
        mainCanvas.drawLine(from: touchStartingPosition, to: touchNewPosition, brushConfiguration: GlobalBrushConfiguration.instance.getBrushConfiguration())
        touchStartingPosition = touchNewPosition
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Hello
        //If undo where to be implemented add behaviour here.
        mainCanvas.updateCanvas()
    }
    
    // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}
