//
//  SingletonPaintConfiguration.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-06.
//  Copyright © 2019 arlw. All rights reserved.
//

//Struct with defaults
struct BrushSettings {
    var brushSize:CGFloat
    var brushType:CGLineCap
    var brushColour:CGColor
    init(size:CGFloat, type:CGLineCap, colour:CGColor) {
        brushSize = size
        brushType = type
        brushColour = colour
    }
}

struct BrushItem {
    let titleName:String
    let type:CGLineCap
}

import Foundation
import CoreGraphics

class GlobalBrushConfiguration {
    static let instance = GlobalBrushConfiguration(size: CGFloat(10), type: .round)
    
    var _brushSize:CGFloat
    var _brushType:CGLineCap
    var _brushColour:CGColor
    
    init(size:CGFloat, type:CGLineCap) {
        self._brushSize = size
        self._brushType = type
        self._brushColour = CGColor(srgbRed: CGFloat(0), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(1))
    }
    func setBrushType(newType:CGLineCap) {
        self._brushType = newType
    }
    func setBrushSize(newSize:CGFloat) {
        self._brushSize = newSize
    }
    func setBrushColour(newColour:CGColor) {
        self._brushColour = newColour
    }
    func saveToUserDefaults() {
        UserDefaults.standard.set(Float(self._brushSize), forKey: "PaintBrushSize")
    }
    func getSavedUserDefaults() -> BrushSettings {
        let savedSize:CGFloat = CGFloat(UserDefaults.standard.float(forKey: "PaintBrushSize"))
        print(savedSize)
        return BrushSettings(size: savedSize, type: .round, colour: CGColor(srgbRed: CGFloat(255), green: CGFloat(255), blue: CGFloat(255), alpha: CGFloat(1)))
    }
    func getBrushConfiguration() -> BrushSettings {
        return BrushSettings(size: self._brushSize, type: self._brushType, colour: self._brushColour)
    }
}
