//
//  ColourAPIRequest.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-08.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation

enum ColorAPIError:Error {
    case noContent
    case unableToProcessData
}

struct ColourSchemeRequest {
    let resourceURL:URL
    
    init(mode:String, colourLimit:Int) {
        let resourceURLString:String = "/scheme?hex=24B1E0&mode=\(mode)&count=\(colourLimit)"
        guard let resourceURL = URL(string: resourceURLString, relativeTo: URL(string: "http://www.thecolorapi.com")) else {
            fatalError("Could not get Colour API!")
        }
        self.resourceURL = resourceURL
    }
    
    func getColourScheme(completion: @escaping(Result<[ColourDetail], ColorAPIError>) -> Void) {
        let task = URLSession.shared.dataTask(with: resourceURL) { data, _, _ in
            guard let jsonData = data else {
                completion(.failure(.noContent))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(ColourResponse.self, from: jsonData)
                let colourDetails = response.response.colours
                completion(.success(colourDetails))
            } catch {
                completion(.failure(.unableToProcessData))
            }
        }
        task.resume()
    }
    
}
