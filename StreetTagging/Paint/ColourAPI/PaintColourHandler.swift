//
//  File.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-08.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation

struct ColourResponse:Decodable {
    var response:ColourScheme
}

struct ColourScheme:Decodable {
    var colours:[ColourDetail]
}
struct ColourDetail:Decodable {
    var name:NameDetail
    var rgb:RGBDetail
    var image:ImageDetail
}
struct NameDetail:Decodable {
    var name:String
    var closest_named_hex:String
}
struct ImageDetail:Decodable {
    var bare:String
    var named:String
}
struct RGBDetail:Decodable {
    var r:Int
    var g:Int
    var b:Int
}

/*class Colour {
    let _red:Int
    let _green:Int
    let _blue:Int
    
    init(red:Int, green:Int, blue:Int) {
        self._red = red
        self._green = green
        self._blue = blue
    }
    init(hex:String) {
        let redString:String = String(hex.prefix(0)) + String(hex.prefix(1))
        let greenString:String = String(hex.prefix(2)) + String(hex.prefix(3))
        let blueString:String = String(hex.prefix(4)) + String(hex.prefix(5))

        self._red = convertHexCharToIntDecimal(hex: Character(String(redString.prefix(0))), index: 5) + convertHexCharToIntDecimal(hex: Character(String(redString.prefix(1))), index: 4)
        self._green = convertHexCharToIntDecimal(hex: Character(String(greenString.prefix(0))), index: 3) + convertHexCharToIntDecimal(hex: Character(String(greenString.prefix(1))), index: 2)
        self._blue = convertHexCharToIntDecimal(hex: Character(String(blueString.prefix(0))), index: 1) + convertHexCharToIntDecimal(hex: Character(String(blueString.prefix(1))), index: 0)
    }
    func getCGColor() -> CGColor {
        return CGColor(srgbRed: CGFloat(self._red), green: CGFloat(self._green), blue: CGFloat(self._blue), alpha: 1);
    }
    func getUIColor() -> UIColor {
        return UIColor(displayP3Red: CGFloat(self._red), green: CGFloat(self._green), blue: CGFloat(self._blue), alpha: 1)
    }
}
func convertHexCharToIntDecimal(hex sign:Character, index:Int) -> Int {
    let hexToInteger:[Character:Int] = [
        "A":10,
        "B":11,
        "C":12,
        "D":13,
        "E":14,
        "F":15
    ]
    var baseValue:Int
    if sign.isWholeNumber {
        baseValue = Int(String(sign)) ?? 0
    } else {
        baseValue = hexToInteger[sign] ?? 0
    }
    let finalValue:Float = Float(baseValue) * powf(Float(16), Float(index))
    
    return Int(finalValue)
}*/
