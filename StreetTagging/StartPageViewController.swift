//
//  StartPageViewController.swift
//  StreetTagging
//
//  Created by Adam Sturesson on 2019-11-18.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit

class StartPageViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkConnectivity.instance.startCheck()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
