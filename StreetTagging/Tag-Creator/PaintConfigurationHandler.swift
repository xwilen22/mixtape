//
//  SingletonPaintConfiguration.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-06.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics


struct BrushSettings {
    var brushSize:CGFloat
    var brushType:CGLineCap
    var brushColour:CGColor
    init(size:CGFloat, type:CGLineCap, colour:CGColor) {
        brushSize = size
        brushType = type
        brushColour = colour
    }
}

struct BrushItem {
    let titleName:String
    let typeImage:UIImage
    let type:CGLineCap
    
    init(titleName:String, type:CGLineCap, typeImage:UIImage?) {
        self.titleName = titleName
        self.type = type
        self.typeImage = typeImage ?? UIImage(systemName: "questionmark")!
    }
}

class GlobalBrushConfiguration {
    static let instance = GlobalBrushConfiguration(size: CGFloat(10), type: .round)
    
    var _brushSize:CGFloat
    var _brushType:CGLineCap
    var _brushColour:CGColor
    var _eraserActive:Bool
    
    init(size:CGFloat, type:CGLineCap) {
        self._brushSize = size
        self._brushType = type
        self._eraserActive = false
        self._brushColour = CGColor(srgbRed: CGFloat(0), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(1))
    }
    func setBrushType(newType:CGLineCap) {
        self._brushType = newType
    }
    func setBrushSize(newSize:CGFloat) {
        self._brushSize = newSize
    }
    func setBrushColour(newColour:CGColor) {
        self._brushColour = newColour
    }
    func setErasing(to enableEraser:Bool) {
        self._eraserActive = enableEraser
    }
    func isEraserActive() -> Bool {
        return self._eraserActive
    }
    func getBrushConfiguration() -> BrushSettings {
        return BrushSettings(size: self._brushSize, type: self._brushType, colour: self._brushColour)
    }
}
