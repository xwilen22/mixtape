//
//  PaintViewController.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-04.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit
import MapKit

class PaintViewController: UIViewController {
    //MARK: - Properties
    var touchStartingPosition:CGPoint = CGPoint.zero
    var swipePerformed:Bool = false
    
    //MARK: - Outlets
    @IBOutlet weak var UIViewMainCanvas: PaintUIViewMainCanvas!
    var mainCanvas:PaintUIViewMainCanvas!
    
    @IBOutlet weak var selectedToolButton: UIButton!
    @IBOutlet weak var selectedColourButton: UIButton!
    //MARK: - View Events
    override func viewDidLoad() {
        super.viewDidLoad()
        mainCanvas = UIViewMainCanvas
        
        selectedColourButton.layer.cornerRadius = selectedColourButton.bounds.size.height * 0.5
        selectedColourButton.layer.borderWidth = 1.5
        
        let currentBrushConfiguration = GlobalBrushConfiguration.instance.getBrushConfiguration()
        
        setUISelectedColour(newColour: UIColor(cgColor: currentBrushConfiguration.brushColour))
    }
    //MARK: - Action Events
    @IBAction func clearCanvasClicked(_ sender: Any) {
        mainCanvas.clearCanvas()
    }
    @IBAction func finishPaintingClicked(_ sender: Any) {
        performSegue(withIdentifier: "unwindToTagViewSegue", sender: nil)
    }
    @IBAction func selectColourClicked(_ sender: Any) {
        performSegue(withIdentifier: "showColourPickerModal", sender: nil)
    }
    @IBAction func selectToolClicked(_ sender: Any) {
        performSegue(withIdentifier: "showToolPickerModal", sender: nil)
    }
    //MARK: - Touch Controls
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchStartingPosition = touches.first!.location(in: mainCanvas)
        swipePerformed = false
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        swipePerformed = true
        let touchNewPosition = touches.first!.location(in: mainCanvas)
        if GlobalBrushConfiguration.instance.isEraserActive() {
            mainCanvas.removeLine(at: touchNewPosition, brushConfiguration: GlobalBrushConfiguration.instance.getBrushConfiguration())
        } else {
            mainCanvas.drawLine(from: touchStartingPosition, to: touchNewPosition, brushConfiguration: GlobalBrushConfiguration.instance.getBrushConfiguration())
        }
        touchStartingPosition = touchNewPosition
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //If user wishes to make a "dot"
        if !swipePerformed {
            if GlobalBrushConfiguration.instance.isEraserActive() {
                mainCanvas.removeLine(at: touchStartingPosition, brushConfiguration: GlobalBrushConfiguration.instance.getBrushConfiguration())
            } else {
                mainCanvas.drawLine(from: touchStartingPosition, to: touchStartingPosition, brushConfiguration: GlobalBrushConfiguration.instance.getBrushConfiguration())
            }
        }
        mainCanvas.updateCanvas()
    }
    //MARK: UI Specific functions
    func setUISelectedTool(typeIcon:UIImage, isEraserSelected:Bool) {
        selectedToolButton.imageView?.image = typeIcon
    }
    func setUISelectedColour(newColour:UIColor) {
        selectedColourButton.backgroundColor = newColour
    }
    func getCanvasImage() -> UIImage {
        return mainCanvas.getUIImage(bounds: mainCanvas.bounds)
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let mapViewController = segue.destination as? MapViewController {
            guard let retrievedImage = sender as? UIImage else {
                return
            }
            mapViewController.addAnnotationAtCurrentLocation(with: retrievedImage)
        }
        if let selectColourControllerModal = segue.destination as? PaintColoursViewController {
            selectColourControllerModal.delegate = self
        }
        if let selectToolControllerModal = segue.destination as? PaintToolsViewController {
            selectToolControllerModal.delegate = self
        }
    }
}
//MARK: - Colour & Tool Select Delegate
extension PaintViewController: SelectedColoursDelegate, SelectedToolsDelegate {
    func newToolSelected(toolImageIcon: UIImage, eraserActive: Bool) {
        setUISelectedTool(typeIcon: toolImageIcon, isEraserSelected: eraserActive)
    }
    func newColourSelected(selectedColour: UIColor) {
        setUISelectedColour(newColour: selectedColour)
    }
}
