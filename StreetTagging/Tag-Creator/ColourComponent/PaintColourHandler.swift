//
//  File.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-08.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation
import UIKit
//MARK: - JSON Decoder Structs
struct ColourResponse:Decodable {
    var response:ColourScheme
}
struct ColourScheme:Decodable {
    var colors:[ColourDetail]
}

struct ColourPalette:Decodable {
    var colors:[ColourDetailMinimal]
}

//Color from name request
struct ColourDetail:Decodable {
    var hex:String
    var name:String
    
    var rgb:RGBDetail
    
    var requestedHex:String
    var luminance:Float
    var distance:Float
}
//Color from get palette request
struct ColourDetailMinimal:Decodable {
    var name:String
    var hex:String
    var rgb:RGBDetail
    var luminance:Float
}
struct RGBDetail:Decodable {
    var r:Int
    var g:Int
    var b:Int
}
//MARK: - Colour interpritation & Session storage
struct ColourItem {
    let colourInfo:ColourDetailMinimal
    let uiColour:UIColor
    init (colour:ColourDetailMinimal) {
        self.colourInfo = colour
        self.uiColour = UIColor(red: CGFloat(colour.rgb.r) / 255, green: CGFloat(colour.rgb.g) / 255, blue: CGFloat(colour.rgb.b) / 255, alpha: CGFloat(1))
    }
}

class GlobalPaletteSessionStorage {
    static let instance = GlobalPaletteSessionStorage()
    var palette:[ColourItem]
    
    init() {
        palette = []
    }
    func getColourPalette() -> [ColourItem] {
        return self.palette
    }
    func setColourPalette(newPalette:[ColourItem]) {
        self.palette = newPalette
    }
}
