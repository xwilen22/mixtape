//
//  PaintColoursViewController.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-04.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit
//MARK: - Delegate protocols
protocol SelectedColoursDelegate {
    func newColourSelected(selectedColour:UIColor)
}

class PaintColoursViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var colourCollection: UICollectionView!
    @IBOutlet weak var colourPreviewView: UIView!
    @IBOutlet weak var colourPreviewName: UILabel!
    //Custom colour sliders
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    @IBOutlet weak var loadingAnimatedObject: UIImageView!
    
    var delegate:SelectedColoursDelegate?
    
    //MARK: - Properties
    var selectedColour:UIColor = UIColor.black
    var paletteColours:[ColourItem] = [ColourItem]() {
        //If a value was set to this list
        didSet {
            DispatchQueue.main.async {
                self.colourCollection.reloadData()
            }
        }
    }
    //MARK: - View Events
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colourCollection.delegate = self
        colourCollection.dataSource = self
        
        selectedColour = UIColor(cgColor: GlobalBrushConfiguration.instance.getBrushConfiguration().brushColour)
        _setRGBSliders(red: Float(selectedColour.getFloatRGB(lightType: .red)), green: Float(selectedColour.getFloatRGB(lightType: .green)), blue: Float(selectedColour.getFloatRGB(lightType: .blue)), maxColourValue: 1)
        _updatePreviewElements(newColour: selectedColour)
    }
    override func viewWillAppear(_ animated: Bool) {
        _initializeAnimationPosition()
        
        paletteColours = GlobalPaletteSessionStorage.instance.getColourPalette()
        
        //Loads only colours if there are none retrieved from memory
        if paletteColours.count <= 0 {
            reloadColourPalette()
        } else {
            _disableLoadingAnimation(instantly: true)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.newColourSelected(selectedColour: selectedColour)
    }
    //MARK: - Class specific functions
    //Used when colour name cannot be guessed (eg. when the user specifies a custom colour)
    private func _updatePreviewElements(newColour:UIColor, getName:Bool = true) {
        colourPreviewView.backgroundColor = newColour
        
        if !getName {
            return
        }
        var request:ColourRequest
        do {
            request = try ColourRequest()
        } catch (let error) {
            let errorMessage:GenericErrorHandler = GenericErrorHandler(title: "Failed to get colour name", message: error.localizedDescription)
            self.present(errorMessage.getAlert(), animated: true, completion: nil)
            return
        }
        
        request.getColour(hex: newColour.hexadecimal()){ [weak self] result in
            switch result {
            case .success(let colour):
                DispatchQueue.main.async {
                    self?.colourPreviewName.text = colour.name
                }
            case .failure(_):
                DispatchQueue.main.async {
                    self?.colourPreviewName.text = "Custom colour"
                }
            }
        }
    }
    //Overload of above function but with a complete ColourItem struct
    private func _updatePreviewElements(newColour:ColourItem) {
        colourPreviewName.text = newColour.colourInfo.name
        colourPreviewView.backgroundColor = newColour.uiColour
    }
    private func _setRGBSliders(to colour:RGBDetail) {
        redSlider.value = Float(colour.r) / 255
        greenSlider.value = Float(colour.g) / 255
        blueSlider.value = Float(colour.b) / 255
    }
    private func _setRGBSliders(red:Float, green:Float, blue:Float, maxColourValue:Float = 255) {
        redSlider.value = red / maxColourValue
        greenSlider.value = green / maxColourValue
        blueSlider.value = blue / maxColourValue
    }
    //MARK: - Animation
    private func _initializeAnimationPosition() {
        self.loadingAnimatedObject.layer.position.y = self.colourCollection.layer.position.y - self.loadingAnimatedObject.bounds.height / 2
        self.loadingAnimatedObject.bounds.size.width = self.colourCollection.bounds.size.width * 0.7
    }
    private func _enableLoadingAnimation() {
        for _ in 1...5 {
            UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn, .repeat, .autoreverse], animations: { [weak self] in
            self?.loadingAnimatedObject.tintColor = UIColor(cgColor: CGColor(srgbRed: CGFloat.random(in: 0...1), green: CGFloat.random(in: 0...1), blue: CGFloat.random(in: 0...1), alpha: 1))
            }, completion: nil)
        }
    }
    private func _disableLoadingAnimation(instantly:Bool) {
        if instantly {
            self.loadingAnimatedObject.alpha = CGFloat(0)
        } else {
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                self?.loadingAnimatedObject.alpha = CGFloat(0)
            })
        }
    }
    //MARK: - Action Events
    @IBAction func buttonDismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func colourValueChanged(_ sender: UISlider) {
        selectedColour = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: CGFloat(1))
        _updatePreviewElements(newColour: selectedColour, getName: false)
        
        GlobalBrushConfiguration.instance.setBrushColour(newColour: selectedColour.cgColor)
    }
    @IBAction func sliderRedConfirmed(_ sender: UISlider) {
        _updatePreviewElements(newColour: selectedColour)
    }
    @IBAction func sliderGreenConfirmed(_ sender: UISlider) {
        _updatePreviewElements(newColour: selectedColour)
    }
    @IBAction func sliderBlueConfirmed(_ sender: UISlider) {
        _updatePreviewElements(newColour: selectedColour)
    }
    
    //MARK: Touch Controls
    //Blocks touch controls occuring on parent view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
}
//MARK: - Collection View
extension PaintColoursViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paletteColours.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = colourCollection.dequeueReusableCell(withReuseIdentifier: "ColourCell", for: indexPath) as? UICollectionViewCell {
            cell.contentView.backgroundColor = paletteColours[indexPath.row].uiColour
            cell.contentView.layer.cornerRadius = 0.5 * cell.bounds.size.height
            cell.contentView.layer.borderColor = CGColor(srgbRed: 0, green: 0, blue: 0, alpha: 0.7)
            cell.contentView.layer.borderWidth = 1
            return cell
        }
        
        return UICollectionViewCell()
    }
    //Cell clicked
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedColour = paletteColours[indexPath.row].uiColour
        _updatePreviewElements(newColour: paletteColours[indexPath.row])
        _setRGBSliders(to: paletteColours[indexPath.row].colourInfo.rgb)
        GlobalBrushConfiguration.instance.setBrushColour(newColour: selectedColour.cgColor)
    }
    //API Request to get colours
    func reloadColourPalette () {
        guard NetworkConnectivity.instance.checkIfConnected() else {
            _disableLoadingAnimation(instantly: true)
            return
        }
        
        var request:ColourRequest
        do {
            request = try ColourRequest()
        } catch (let error) {
            let errorMessage:GenericErrorHandler = GenericErrorHandler(title: "Failed to load colour-palette", message: error.localizedDescription)
            self.present(errorMessage.getAlert(), animated: true, completion: nil)
            return
        }
        
        _enableLoadingAnimation()
        request.getColourPalette() { [weak self] result in
            switch (result) {
            case .success(let palette):
                var newPalette:[ColourItem] = [ColourItem]()
                
                for colour in palette {
                    newPalette.append(ColourItem(colour: colour))
                }
                GlobalPaletteSessionStorage.instance.setColourPalette(newPalette: newPalette)
                self?.paletteColours = newPalette
            case .failure(let error):
                let errorMessage:GenericErrorHandler = GenericErrorHandler(title: "It's not you, it's us", message: error.localizedDescription)
                self?.present(errorMessage.getAlert(), animated: true, completion: nil)
            }
            //Hides loading screen
            DispatchQueue.main.async {
                self?._disableLoadingAnimation(instantly: false)
            }
        }
    }
}
//MARK: - UIColor extension
extension UIColor {
    func hexadecimal() -> String {
        var red:CGFloat = getFloatRGB(lightType: .red), green:CGFloat = getFloatRGB(lightType: .green), blue:CGFloat = getFloatRGB(lightType: .blue)
        //RRGGBB
        //floor(R / 16) = Left val
        //R mod 16 = Right Val
        //Format string for a single decimal value to a single hexadecimal
        let hexFormater:String = "%01X"
        if red > 1 {
            red = 1.0
        }
        if green > 1 {
            green = 1.0
        }
        if blue > 1 {
            blue = 1.0
        }
        // *255 since all the rgb components are represented by a floating point value 0 - 1
        let redComponent:String = String(format: hexFormater, Int(floor(abs(red * 255 / 16)))) + String(format: hexFormater, Int(fmod(abs(red * 255), 16)))
        let greenComponent:String = String(format: hexFormater, Int(floor(abs(green * 255 / 16)))) + String(format: hexFormater, Int(fmod(abs(green * 255), 16)))
        let blueComponent:String = String(format: hexFormater, Int(floor(abs(blue * 255 / 16)))) + String(format: hexFormater, Int(fmod(abs(blue * 255), 16)))
        
        return String("#" + redComponent + greenComponent + blueComponent)
    }
    enum _redGreenBlueSelector {
        case red
        case green
        case blue
    }
    func getFloatRGB(lightType:_redGreenBlueSelector) -> CGFloat {
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if !getRed(&r, green: &g, blue: &b, alpha: &a) {
            return 0
        } else {
            switch lightType {
            case .red:
                return r
            case .green:
                return g
            case .blue:
                return b
            }
        }
    }
}
