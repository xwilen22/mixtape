//
//  ColourAPIRequest.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-08.
//  Copyright © 2019 arlw. All rights reserved.
//

import Foundation

enum ColourAPIError:Error {
    case noContent
    case unableToProcessData
    case invalidRequest
    case apiUnavailable
    case unkown
}

struct ColourRequest {
    let resourceBaseURL:URL
    
    init() throws {
        guard let resourceURL:URL = URL(string: "https://api.color.pizza/v1/") else {
            throw ColourAPIError.unkown
        }
        self.resourceBaseURL = resourceURL
    }
    func getColour(hex:String ,completion: @escaping(Result<ColourDetail, ColourAPIError>) -> Void) {
        var cleanHex:String = hex
        //If the first character contains a # then the hex gets cleaned up
        if hex.prefix(1) == "#" {
            cleanHex = String(hex.components(separatedBy: "#")[1])
        }
        if cleanHex.count != 6 {
            completion(.failure(.invalidRequest))
            return
        }
        let task = URLSession.shared.dataTask(with: URL(string: cleanHex, relativeTo: resourceBaseURL)!) { data, response, error in
            guard let sessionResponse = response as? HTTPURLResponse else {
                completion(.failure(.apiUnavailable))
                return
            }

            if sessionResponse.statusCode != 200 {
                completion(.failure(.apiUnavailable))
            }
            
            guard let jsonData = data else {
                completion(.failure(.noContent))
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(ColourScheme.self, from: jsonData)
                completion(.success(response.colors[0]))
            } catch {
                completion(.failure(.unableToProcessData))
            }
        }
        task.resume()
    }
    func getColourPalette(completion: @escaping(Result<[ColourDetailMinimal], ColourAPIError>) -> Void) {
        let task = URLSession.shared.dataTask(with: resourceBaseURL) { data, response, error in
            guard let sessionResponse = response as? HTTPURLResponse else {
                completion(.failure(.apiUnavailable))
                return
            }

            if sessionResponse.statusCode != 200 {
                completion(.failure(.apiUnavailable))
            }
            
            guard let jsonData = data else {
                completion(.failure(.noContent))
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(ColourPalette.self, from: jsonData)
                completion(.success(response.colors))
            } catch {
                completion(.failure(.unableToProcessData))
            }
        }
        task.resume()
    }
    
}
