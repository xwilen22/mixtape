//
//  BrushSelectorUISegmentedControl.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-06.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit

class BrushSelectorUISegmentedControl: UISegmentedControl {
    func setBrushes(items:[BrushItem], selectedIndex:Int?) {
        self.removeAllSegments()
        for (index, brush) in items.enumerated() {
            self.insertSegment(withTitle: brush.titleName, at: index, animated: true)
        }
        self.selectedSegmentIndex = selectedIndex ?? 0
    }
}
