//
//  PaintToolsViewController.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-04.
//  Copyright © 2019 arlw. All rights reserved.
//
import UIKit

//MARK: - Delegate protocols
protocol SelectedToolsDelegate {
    func newToolSelected(toolImageIcon:UIImage, eraserActive:Bool)
}

class PaintToolsViewController: UIViewController {
    //MARK: - Properties
    let availableBrushes:[BrushItem] = [
        BrushItem(titleName: "Round", type: .round, typeImage: UIImage(named: "ToolRound")),
        BrushItem(titleName: "Square", type: .square, typeImage: UIImage(named: "ToolSquare")),
        BrushItem(titleName: "Butt", type: .butt, typeImage: UIImage(named: "ToolButt"))
    ]
    var previewSettings:BrushSettings = GlobalBrushConfiguration.instance.getBrushConfiguration()
    var delegate:SelectedToolsDelegate?
    var selectedBrushIndex:Int = 0
    //MARK: - Outlets
    @IBOutlet weak var brushSelector: BrushSelectorUISegmentedControl!
    @IBOutlet weak var brushSizeUISlider: UISlider!
    @IBOutlet weak var eraserSwitch: UISwitch!
    @IBOutlet weak var previewUIView: PaintUIViewMainCanvas!
    //MARK: - View Events
    override func viewDidLoad() {
        super.viewDidLoad()
        //Finds index of currently selected brush and adds all available brushitems to segmentedcontrol
        for (index, brush) in availableBrushes.enumerated() {
            if brush.type == previewSettings.brushType {
                brushSelector.setBrushes(items: availableBrushes, selectedIndex: index)
                selectedBrushIndex = index
                break
            }
        }
        brushSizeUISlider.setValue(Float(previewSettings.brushSize), animated: true)
        eraserSwitch.isOn = GlobalBrushConfiguration.instance.isEraserActive()
    }
    override func viewDidAppear(_ animated: Bool) {
        _updateBrushPreview(with: previewSettings)
    }
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.newToolSelected(toolImageIcon: availableBrushes[selectedBrushIndex].typeImage, eraserActive: eraserSwitch.isOn)
    }
    //MARK: - Control Events
    @IBAction func buttonDismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func paintBrushChanged(_ sender: UISegmentedControl) {
        previewSettings.brushType = availableBrushes[sender.selectedSegmentIndex].type
        GlobalBrushConfiguration.instance.setBrushType(newType: availableBrushes[sender.selectedSegmentIndex].type)
        
        _updateBrushPreview(with: previewSettings)
    }
    @IBAction func brushSizeChanged(_ sender: UISlider) {
        previewSettings.brushSize = CGFloat(sender.value)
        GlobalBrushConfiguration.instance.setBrushSize(newSize: CGFloat(sender.value))

        _updateBrushPreview(with: previewSettings)
    }
    @IBAction func eraserSwitchFlipped(_ sender: UISwitch) {
        GlobalBrushConfiguration.instance.setErasing(to: sender.isOn)
    }
    // MARK: Touch events
    //Blocks touch controls occuring on parent view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    //MARK: Class Specific Functions
    private func _updateBrushPreview(with configuration:BrushSettings) {
        previewUIView.clearCanvas()
        let from:CGPoint = CGPoint(x: CGFloat(20), y: previewUIView.bounds.height / 2)
        let to:CGPoint = CGPoint(x: previewUIView.bounds.width - CGFloat(20), y: previewUIView.bounds.height / 2)
        previewUIView.drawLine(from: from, to: to, brushConfiguration: configuration)
    }
}
