//
//  PaintUIViewMainCanvas.swift
//  StreetTagging
//
//  Created by William Segelström on 2019-11-05.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit

class PaintUIViewMainCanvas: UIView {
    //MARK: - Properties
    struct LinePosition {
        let from:CGPoint
        let to:CGPoint
        
        let styling:BrushSettings
    }
    //All lines in canvas
    var canvasLines:[LinePosition] = []
    //MARK: - View events
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            print("Could not find context! Aborting draw action")
            return
        }
        
        for line in canvasLines {
            context.move(to: line.from)
            context.addLine(to: line.to)
            
            context.setLineCap(line.styling.brushType)
            context.setLineWidth(line.styling.brushSize)
            context.setStrokeColor(line.styling.brushColour)
            
            context.strokePath()
        }
    }
    //MARK: - Canvas Functions
    func clearCanvas() {
        canvasLines.removeAll()
        updateCanvas()
    }
    func updateCanvas() {
        setNeedsDisplay()
    }
    func removeLine(at:CGPoint, brushConfiguration:BrushSettings) {
        let brushSize:CGSize = CGSize(width: brushConfiguration.brushSize, height: brushConfiguration.brushSize)
        let removalRect:CGRect = CGRect(origin: at, size: brushSize)
        
        let additionalMargin:CGFloat = CGFloat(10)
        
        for (index, line) in canvasLines.enumerated() {
            if removalRect.contains(line.from) || removalRect.contains(line.to) {
                if index < canvasLines.count {
                    canvasLines.remove(at: index)
                    setNeedsDisplay(_getUpdateAreaRect(topCorner: line.from, bottomCorner: line.to, sizeMargin: line.styling.brushSize + additionalMargin))
                }
            }
        }
    }
    func drawLine(from:CGPoint, to:CGPoint, brushConfiguration:BrushSettings) {
        canvasLines.append(LinePosition(from: from, to: to, styling: brushConfiguration))
        
        let updateArea:CGRect = _getUpdateAreaRect(topCorner: from, bottomCorner: to, sizeMargin: brushConfiguration.brushSize)
        setNeedsDisplay(updateArea)
    }
    func getUIImage(bounds: CGRect) -> UIImage {
        let imageRenderer = UIGraphicsImageRenderer(size: bounds.size)
        let image:UIImage = imageRenderer.image(actions: {
            context in self.drawHierarchy(in: bounds, afterScreenUpdates: true)
        })
        return image
    }
    //MARK: View Specific functions
    //Decides which part of the canvas to re-render
    private func _getUpdateAreaRect(topCorner:CGPoint, bottomCorner:CGPoint, sizeMargin:CGFloat) -> CGRect {
        var sizeMarginHeight:CGFloat = sizeMargin, sizeMarginWidth:CGFloat = sizeMargin
        
        //If difference between coordinates are negative the size margin should follow suit
        if (topCorner.x - bottomCorner.x) < 0 {
            sizeMarginWidth *= (-1)
        }
        if (topCorner.y - bottomCorner.y) < 0 {
            sizeMarginHeight *= (-1)
        }
        //Rectangle origin in the center between the two points
        let updateAreaOrigin:CGPoint = CGPoint(x: (topCorner.x + bottomCorner.x - sizeMarginWidth) / 2, y: (topCorner.y + bottomCorner.y - sizeMarginHeight) / 2)
        let updateAreaSize:CGSize = CGSize(width: (topCorner.x - bottomCorner.x) + sizeMarginWidth, height: (topCorner.y - bottomCorner.y) + sizeMarginHeight)
        
        return CGRect(origin: updateAreaOrigin, size: updateAreaSize)
    }
}
