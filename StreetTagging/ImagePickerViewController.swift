//
//  ImagePickerViewController.swift
//  StreetTagging
//
//  Created by Ludwig Åkermark on 2019-11-19.
//  Copyright © 2019 arlw. All rights reserved.
//

import UIKit
import Photos
import UserNotifications
class ImagePickerViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: - Outlets
    @IBOutlet weak var drawNewButton: UIButton!
    @IBOutlet weak var chooseExistingButton: UIButton!
    // MARK: - Properties
    var imagePicker:UIImagePickerController = UIImagePickerController()
    var pickedImage:UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Making buttons less edgy i.e. rounds edges
        drawNewButton.layer.cornerRadius = 0.2 * drawNewButton.bounds.height
        chooseExistingButton.layer.cornerRadius = 0.2 * chooseExistingButton.bounds.height
    }
    //MARK: - Action Events
    //  Choose existing tag button clicked
    //  checks if device supports the photo album and presents it to the viewer
    @IBAction func chooseBtn(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
           imagePicker.delegate = self
           imagePicker.sourceType = .savedPhotosAlbum
           imagePicker.allowsEditing = false
           present(imagePicker, animated: true, completion: nil)
       }
    }
    //Unwind from tag-creator since you need to unwind the nav-controller before unwinding the tab-bar controller
    @IBAction func unwindToTagAddViewController(_ unwindSegue: UIStoryboardSegue) {
        guard let sourceViewController = unwindSegue.source as? PaintViewController else {
            return
        }
        // Use data from the view controller which initiated the unwind segue
        let customImage:UIImage = sourceViewController.getCanvasImage()
        performSegue(withIdentifier: "unwindToMapViewSegue", sender: customImage)
    }
    //  image has been selected, converts the picked image to a UIimage and segues it over to the map view.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            dismiss(animated: true, completion: nil)
            self.performSegue(withIdentifier: "unwindToMapViewSegue", sender: pickedImage)
        }
    }
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let retrievedImage = sender as? UIImage else {
             return
         }
         guard let mapViewController = segue.destination as? MapViewController else {
             return
         }
         mapViewController.addAnnotationAtCurrentLocation(with: retrievedImage)

    }
}
